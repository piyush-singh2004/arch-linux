set fish_greeting                                 # Supresses fish's intro message
set TERM "xterm-256color"                         # Sets the terminal type

### ALIASES ###
## Exa Aliases For File listing ##
alias ls="exa -lhg --all --all --icons --group-directories-first"
alias ll="exa --icons --group-directories-first -l"
## Emacs Aliases ##
alias emacs="emacs -nw"
## APT Aliases ##
alias update="sudo apt update"
alias upgrade="sudo apt upgrade -y"
alias uptodate="sudo apt update && sudo apt upgrade"
alias install="sudo apt install"
alias exercism="exercism.exe"
alias doom="~/.emacs.d/bin/doom"

# cd /mnt/c/Users/VINAY/Documents/

starship init fish | source

# export DISPLAY=172.26.144.1:0
